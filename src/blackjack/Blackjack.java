/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blackjack;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Naményi Zsolt
 */
public class Blackjack {

    static final int n = 52;

    public static void main(String[] args) {
        String loadedCards[] = new String[n];

        String SamCards[] = new String[n];
        String DealerCards[] = new String[n];

        // Init cards array
        int loaded = 0;
        try {
            RandomAccessFile f = new RandomAccessFile("playingcards.txt", "r");
            for (String s = f.readLine(); s != null; s = f.readLine()) {
                StringTokenizer token = new StringTokenizer(s, ", ");
                while (token.hasMoreTokens() && loaded < n) {
                    loadedCards[loaded] = token.nextToken();
                    loaded++;
                }
            }
        } catch (FileNotFoundException ex) {
            // If file not found, initalized with random values
            for (int i = 0; i < n; i++) {
                loadedCards = generateDeckOfPlayingCards();
            }
            // Mix it
            shuffleArray(loadedCards);
        } catch (IOException ex) {
            Logger.getLogger(Blackjack.class.getName()).log(Level.SEVERE, null, ex);
        }
        // After initalized
        for (int i = 0; i < n; i++) {
            if (loadedCards[i] == null) {
                System.err.println("Critical error: loadedCards array contains null value.");
                System.exit(0);
                break;
            }
        }

        SamCards[0] = selectCard(loadedCards);
        DealerCards[0] = selectCard(loadedCards);
        SamCards[1] = selectCard(loadedCards);
        DealerCards[1] = selectCard(loadedCards);
        //check if either player has Blackjack with their initial hand
        String winner = null;
        if (getValue(SamCards) == 21 && getValue(DealerCards) != 21) {
            winner = "Sam";
        } else if (getValue(SamCards) == 21 && getValue(DealerCards) == 21) { // sam wins when both players starts with Blackjack 
            winner = "Sam";
        } else if (getValue(SamCards) != 21 && getValue(DealerCards) == 21) {
            winner = "Dealer";
        } else if (getValue(SamCards) == 22 && getValue(DealerCards) == 22) { // dealer wins when both players starts with 22
            winner = "Dealer";
        } else {
            int x = 2;
            while (getValue(SamCards) < 17 && countSelectableCards(loadedCards) > 0) {
                SamCards[x] = selectCard(loadedCards);
                x++;
            }
            if (getValue(SamCards) == 21) {
                winner = "Sam";
            } else if (getValue(SamCards) > 21) {
                winner = "Dealer";
            } else {
                x = 2;
                while (getValue(DealerCards) < getValue(SamCards) && countSelectableCards(loadedCards) > 0) {
                    DealerCards[x] = selectCard(loadedCards);
                    x++;
                }
                if (winner == null) {
                    if (getValue(DealerCards) <= getValue(SamCards) && getValue(SamCards) <= 21) {
                        winner = "Sam";
                    } else if (getValue(DealerCards) > getValue(SamCards) && getValue(DealerCards) <= 21) {
                        winner = "Dealer";
                    } else {
                        winner = "Sam";
                    }
                }
            }
        }
        System.out.println(winner + " has won the game.");
        System.out.println("[sam|dealer]");
        System.out.print("sam: ");
        for (int i = 0; i < n; i++) {
            if (SamCards[i] == null) {
                continue;
            }
            System.out.print(SamCards[i] + ", ");
        }
        System.out.println("");
        System.out.print("dealer: ");
        for (int i = 0; i < n; i++) {
            if (DealerCards[i] == null) {
                continue;
            }
            System.out.print(DealerCards[i] + ", ");
        }
        System.out.println("");
    }

    static int countSelectableCards(String[] array) {
        int number = 0;
        for (String item : array) {
            if (item != null) {
                number++;
            }
        }
        return number;
    }

    static int getValue(String player[]) {
        int points = 0;
        for (String item : player) {
            if (item != null) {
                points += getPointFromCard(item);
            }
        }
        return points;
    }

    static int getPointFromCard(String card) {
        int point = 0;
        switch (card.substring(1)) {
            case "J":
            case "Q":
            case "K":
                point = 10;
                break;
            case "A":
                point = 11;
                break;
            default:
                point = Integer.parseInt(card.substring(1));
                break;
        }
        return point;
    }

    static String selectCard(String[] array) {
        boolean hasCard = false;
        int topIndex = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                hasCard = true;
                topIndex = i;
                break;
            }
        }
        if (hasCard == true && topIndex != -1) {
            String selectedCard = array[topIndex];
            array[topIndex] = null;
            return selectedCard;
        }
        return null;
    }

    /*static String selectCard(String[] array) {
        boolean hasCard = false;
        for (String array_item : array) {
            if (array_item != null) {
                hasCard = true;
                break;
            }
        }
        if (hasCard == true) {
            String selectedCard = null;
            while (selectedCard == null) {
                int random = randomInt(0, n - 1);
                if (array[random] != null) {
                    selectedCard = array[random];
                    array[random] = null;
                }
            }
            return selectedCard;
        }
        return null;
    }*/
    static int randomInt(int low, int high) {
        Random r = new Random();
        return r.nextInt(high - low) + low;
    }

    static String[] generateDeckOfPlayingCards() {
        String deck[] = new String[n];
        int x = 0;
        while (x != 4) {
            for (int i = 0; i < n / 4; i++) {
                // Set the type of card
                char typeOfCard = 'S';
                switch (x) {
                    case 1:
                        typeOfCard = 'C';
                        break;
                    case 2:
                        typeOfCard = 'D';
                        break;
                    case 3:
                        typeOfCard = 'H';
                        break;
                }
                // Change name to Jack, Quenn, King or Ace and fix the numbers
                String numberOfCard = String.valueOf(i + 2);
                if (i == 9) {
                    numberOfCard = "J";
                } else if (i == 10) {
                    numberOfCard = "Q";
                } else if (i == 11) {
                    numberOfCard = "K";
                } else if (i == 12) {
                    numberOfCard = "A";
                }
                deck[i + ((n / 4) * x)] = String.valueOf(typeOfCard) + String.valueOf(numberOfCard);
            }
            x++;
        }
        return deck;
    }

    static void shuffleArray(String[] ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            String a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

}
